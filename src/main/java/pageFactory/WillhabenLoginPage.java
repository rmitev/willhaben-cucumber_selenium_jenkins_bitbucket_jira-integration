package pageFactory;

import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.PageFactory;

public class WillhabenLoginPage {

    private WebDriver driver;

    @FindBy(id = "email")
    private WebElement inputFieldEMail;

    @FindBy(id="password")
    private WebElement inputFieldPassword;

    @FindBy(xpath = "//span[contains(text(),'Einloggen')]")
    private WebElement submitButton;

    //alert message: Bitte gib eine gültige E-Mail-Adresse an
    @FindBy(id = "email-message")
    private WebElement alertIncorrectEMailFormat;

//    @FindBy(xpath = "//p[contains(text(),'Der Benutzername oder das Passwort konnten nicht e')]") //still OK
    @FindBy(xpath = "//body/div[1]/div[1]/div[1]/div[2]/div[1]/div[1]/div[1]/div[1]/p[1]")
    private WebElement alertIncorrectCredentials;

    //alert message: Bitte gib dein Passwort ein
    @FindBy(id = "password-message")
    private WebElement alertNoPasswordAdded;

    public WillhabenLoginPage(WebDriver driver) {
        this.driver = driver;
        PageFactory.initElements(driver, this);
    }

    public void setEMail(String eMail) {
        this.inputFieldEMail.sendKeys(eMail);
    }

    public void setPassword(String inputFieldPassword) {
        this.inputFieldPassword.sendKeys(inputFieldPassword);
    }

    public void setSubmitButtonClick() {
        this.submitButton.click();
    }

    public WebElement getAlertIncorrectCredentials() {
        return this.alertIncorrectCredentials;
    }

    public WebElement getAlertIncorrectEMailFormat() {
        return this.alertIncorrectEMailFormat;
    }

    public WebElement getSubmitButton() {
        return this.submitButton;
    }

    public WebElement getAlertNoPasswordAdded() {
        return this.alertNoPasswordAdded;
    }
}
